﻿
namespace CKAKinect2Tool
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using Microsoft.Kinect;
    using Emgu.CV;
    using Emgu.CV.Structure;
    using System.Windows.Controls;
    using System.Threading;
    using System.Media;
    using System.Text;

    public partial class MainWindow : Window
    {

        string ui_scriptPath = null;
        string ui_userListPath = null;
        bool ui_IsRecording = false;
        string ui_scriptID = null;

        List<Gesture> ui_script = null;
        List<String> ui_scriptList = null;
        List<User> ui_users = null;
        List<String> ui_userList = null;

        #region DISPLAY
        private void ui_displayRadioButtons_Checked(object sender, RoutedEventArgs e)
        {
            if (this.cbDrawBody == null || this.rbColor == null || this.rbDepth == null || this.rbInfrared == null || this.rbBodyIndex == null) return;

            // Must check the cbDrawBody first.
            if (this.cbDrawBody.IsChecked == true)
            {
                this.imgDisplayImage.Source = this.bodyImageSource;
            } 
            else if (this.rbColor.IsChecked == true)
            {
                this.imgDisplayImage.Source = this.colorImageSource;
            }
            else if (this.rbDepth.IsChecked == true)
            {
                this.imgDisplayImage.Source = this.depthImageSource;
            }
            else if (this.rbInfrared.IsChecked == true)
            {
                this.imgDisplayImage.Source = this.infraredImageSource;
            }
            else if (this.rbBodyIndex.IsChecked == true)
            {
                this.imgDisplayImage.Source = this.bodyIndexImageSource;
            }
        }

        private void ui_cbDrawBody_Checked(object sender, RoutedEventArgs e)
        {
            this.imgDisplayImage.Source = this.bodyImageSource;
        }

        private void ui_cbDrawBody_Unchecked(object sender, RoutedEventArgs e)
        {
            if (this.rbColor.IsChecked == true)
            {
                this.imgDisplayImage.Source = this.colorImageSource;
            }
            else if (this.rbDepth.IsChecked == true)
            {
                this.imgDisplayImage.Source = this.depthImageSource;
            }
            else if (this.rbInfrared.IsChecked == true)
            {
                this.imgDisplayImage.Source = this.infraredImageSource;
            }
            else if (this.rbBodyIndex.IsChecked == true)
            {
                this.imgDisplayImage.Source = this.bodyIndexImageSource;
            }
        }
        #endregion
        
        #region BUTTONS
        private void ui_btnOpenUserWindow(object sender, RoutedEventArgs e1)
        {
            if (this.userGestureWindow == null)
            {
                this.userGestureWindow = new GestureWindow(new VisualBrush(this.meControllerGesturePlayer));
                this.userGestureWindow.tbGestureName.Text = this.ui_script[this.lbGestureNames.SelectedIndex].Name;
                this.userGestureWindow.Show();
                this.btnOpenUserWindow.Content = "Close User Window";
            }
            else
            {
                this.userGestureWindow.Close();
                this.userGestureWindow = null;
                this.btnOpenUserWindow.Content = "Open User Window";
            }
        }

        private void ui_btnReplayVideo_Click(object sender, RoutedEventArgs e)
        {
            this.lbGestureNames.SelectedIndex = this.lbGestureNames.SelectedIndex + 1;
            this.lbGestureNames.SelectedIndex = this.lbGestureNames.SelectedIndex - 1;
        }

        private void ui_btnRecord_Click(object sender, RoutedEventArgs e)
        {
            Thread.Sleep(100);

            if (ui_IsRecording == false)
            {
                record_startRecordingFunction();
            }
            else // To Stop the Recording
            {
                record_stopRecordingFunction();
            }
        }
        
        private void ui_btnStartGesture_Click(object sender, RoutedEventArgs e)
        {   
            this.userGestureWindow.gestureGrid.Background = Brushes.ForestGreen;
            this.gridRecordControls.Background = Brushes.ForestGreen;

            this.record_Labels_csv.Append(removeSpecialCharacters(this.ui_scriptList[this.lbGestureNames.SelectedIndex]) + ";");
            this.record_Labels_csv.Append(this.record_nFrames + ";");
        }

        private void ui_btnStopGesture_Click(object sender, RoutedEventArgs e)
        {
            this.userGestureWindow.gestureGrid.Background = Brushes.Gray;
            this.gridRecordControls.Background = Brushes.Gray;
            
            this.record_Labels_csv.Append(this.record_nFrames + ";Valid\n");
        }

        private void ui_btnNextGesture_Click(object sender, RoutedEventArgs e)
        {
            this.lbGestureNames.SelectedIndex = this.lbGestureNames.SelectedIndex + 1;
            this.lbGestureNames.ScrollIntoView(this.lbGestureNames.SelectedItem);
            this.userGestureWindow.gestureGrid.Background = Brushes.Goldenrod;
            this.gridRecordControls.Background = Brushes.Goldenrod;
            this.gbRecordingModalities.Header = "Gestures (" + (this.lbGestureNames.SelectedIndex + 1) + "/" + this.lbGestureNames.Items.Count.ToString() + ")";
        }

        private void ui_btnPrevGesture_Click(object sender, RoutedEventArgs e)
        {
            if (this.lbGestureNames.SelectedIndex > 0) { 
                this.lbGestureNames.SelectedIndex = this.lbGestureNames.SelectedIndex - 1;
                this.lbGestureNames.ScrollIntoView(this.lbGestureNames.SelectedItem);
                this.userGestureWindow.gestureGrid.Background = Brushes.Orange;
                this.gridRecordControls.Background = Brushes.Orange;
                this.gbRecordingModalities.Header = "Gestures (" + (this.lbGestureNames.SelectedIndex + 1) + "/" + this.lbGestureNames.Items.Count.ToString() + ")";
            }
        }

        private void ui_btnRepeatGesture_Click (object sender, RoutedEventArgs e)
        {
            this.lbGestureNames.SelectedIndex = this.lbGestureNames.SelectedIndex + 1;
            this.lbGestureNames.SelectedIndex = this.lbGestureNames.SelectedIndex - 1;
            this.userGestureWindow.gestureGrid.Background = Brushes.Orange;
            this.gridRecordControls.Background = Brushes.Orange;
        }

        private void ui_btnInvalidGesture_Click (object sender, RoutedEventArgs e)
        {
            this.record_Labels_csv.Length = this.record_Labels_csv.Length - 6;
            this.record_Labels_csv.Append("Invalid\n");
        }
                
        #endregion   
                
        #region RECORDING OPTIONS
        private void ui_tbBrowseScript_DoubleClick(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".script";
            dlg.Filter = "Script Files (*.script)|*.script";
            
            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();
            
            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                // Open document 
                string filename = dlg.FileName;

                // Script ID
                this.ui_scriptID = Path.GetFileNameWithoutExtension(filename);

                // Set the ScriptPath Variable
                this.ui_scriptPath = filename;

                // Change BrowseScript TextBox Text
                this.tbBrowseScript.Text = Path.GetFileNameWithoutExtension(filename);

                // Enable Load Script Button
                this.btnLoadScript.IsEnabled = true;
            }
        }

        private void ui_tbBrowseUserList_DoubleClick(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".userlist";
            dlg.Filter = "User List Files (*.userlist)|*.userlist";

            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                // Open document 
                string filename = dlg.FileName;

                // Set the UserListPath variable
                this.ui_userListPath = filename;

                // Change BrowseUserList TextBox Text
                this.tbBrowseUserList.Text = Path.GetFileNameWithoutExtension(filename);

                // Enable the Load User List Button
                this.btnLoadUserList.IsEnabled = true;
            }
        }
        
        private void ui_btnLoadScript_Click(object sender, RoutedEventArgs e)
        {
            this.ui_script = new List<Gesture>();
            this.ui_scriptList = new List<String>();

            FileStream myFileStream = File.OpenRead(this.ui_scriptPath);
            StreamReader reader = new StreamReader(myFileStream);
            
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                var values = line.Split(';');

                Gesture tempGesture = new Gesture((string)values[0], (string)values[1]);
                this.ui_script.Add(tempGesture);
                this.ui_scriptList.Add(tempGesture.Name);
            }

            this.lbGestureNames.SelectedIndex = 0;
            this.lbGestureNames.ItemsSource = this.ui_scriptList;
            this.tbGestureName.Text = this.ui_script[0].Name;

            this.btnOpenUserWindow.IsEnabled = true;
            this.btnReplayGesture.IsEnabled = true;

            this.gbRecordingModalities.Header = "Gestures (" + (this.lbGestureNames.SelectedIndex + 1) + "/" + this.lbGestureNames.Items.Count.ToString() + ")" ;
        }
        
        private void ui_btnLoadUserList_Click(object sender, RoutedEventArgs e)
        {
            this.ui_users = new List<User>();
            this.ui_userList = new List<String>();

            FileStream myFileStream = File.OpenRead(this.ui_userListPath);
            StreamReader reader = new StreamReader(myFileStream);

            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                var values = line.Split(';');

                User tempUser = new User((string) values[0], (string) values[1]);
                this.ui_users.Add(tempUser);
                this.ui_userList.Add(tempUser.Name);
            }

            this.cmbUserList.SelectedIndex = 0;
            this.cmbUserList.ItemsSource = this.ui_userList;

        }
        #endregion
        
        #region GESTURE LIST
        private void ui_lbGestureNames_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try  {
                this.meControllerGesturePlayer.Source = new Uri(ui_script[this.lbGestureNames.SelectedIndex].VideoPath);
                this.tbGestureName.Text = this.ui_script[this.lbGestureNames.SelectedIndex].Name;
                if (this.userGestureWindow != null)
                {
                    this.userGestureWindow.tbGestureName.Text = this.ui_script[this.lbGestureNames.SelectedIndex].Name;
                }
                this.gbRecordingModalities.Header = "Gestures (" + (this.lbGestureNames.SelectedIndex + 1) + "/" + this.lbGestureNames.Items.Count.ToString() + ")";
            }
            catch (System.ArgumentOutOfRangeException exp)
            {
                Console.WriteLine(exp.StackTrace);
            }
        }
        #endregion 

        public string removeSpecialCharacters(string myString)
        {
            myString = myString.Replace("ç", "c"); myString = myString.Replace("Ç", "C");
            myString = myString.Replace("ğ", "g"); myString = myString.Replace("Ğ", "G");
            myString = myString.Replace("ı", "i"); myString = myString.Replace("İ", "I");
            myString = myString.Replace("ö", "o"); myString = myString.Replace("Ö", "O");
            myString = myString.Replace("ş", "s"); myString = myString.Replace("Ş", "S");
            myString = myString.Replace("ü", "u"); myString = myString.Replace("Ü", "U");

            return myString;
        }
    }
}