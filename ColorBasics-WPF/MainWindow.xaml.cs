﻿
namespace CKAKinect2Tool
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using Microsoft.Kinect;
    using Emgu.CV;
    using Emgu.CV.Structure;
    using System.Windows.Controls;
    using System.Threading;

    public partial class MainWindow : Window
    {
        // The Second Screen
        GestureWindow userGestureWindow = null;

        /// Active Kinect sensor
        private KinectSensor kinectSensor = null;

        /// Size of the RGB pixel in the bitmap
        private readonly int bytesPerPixel = (PixelFormats.Bgr32.BitsPerPixel + 7) / 8;

        // MultiSourceFrame Variables
        MultiSourceFrameReader displayMultiSourceFrameReader = null;
        MultiSourceFrameReader recordMultiSourceFrameReader = null;
        FrameSourceTypes defaultDisplaySources = FrameSourceTypes.Color | FrameSourceTypes.Depth | FrameSourceTypes.Infrared | FrameSourceTypes.Body | FrameSourceTypes.BodyIndex;
    
        #region Variables
        
        // Color Image Variables
        public WriteableBitmap colorImageBitmap = null;
        private byte[] colorImage = null;
        private byte[] colorImageYCC = null;
        private Emgu.CV.Image<Bgr, Byte> record_colorFrameBGR = null;
        private Emgu.CV.Image<Bgra, Byte> record_colorFrameBGRA = null;
        
        // Infrared Image Variables
        private WriteableBitmap infraredImageBitmap = null;
        private byte[] infraredPixels = null;
        private ushort[] infraredFrameData = null;
        
        // Depth Image Variables
        private WriteableBitmap depthImageBitmap = null;
        private byte[] depthPixels = null;
        private ushort[] depthFrameData = null;
        
        // Body Variables
        private DrawingGroup drawingGroup;
        private DrawingImage bodyDrawingImage;
        private CoordinateMapper coordinateMapper = null;
        private Body[] bodies = null;

        // Body Index Variables
        private WriteableBitmap bodyIndexImageBitmap = null;
        private byte[] displayPixels = null;
        private byte[] bodyIndexFrameData = null;
        private ColorSpacePoint[] colorPoints = null;

        #endregion

        /// Initializes a new instance of the MainWindow class.
        public MainWindow()
        {

            // To make sure your thread does not get terminated! (very important!)
            Process.GetCurrentProcess().PriorityClass = System.Diagnostics.ProcessPriorityClass.RealTime;
            Thread.CurrentThread.Priority = ThreadPriority.Highest;
            
            // Get the sensor
            this.kinectSensor = KinectSensor.GetDefault();
            
            if (this.kinectSensor != null)
            {
                // open the sensor
                this.kinectSensor.Open();
                                
                // Display Multi Source Frame Reader
                this.displayMultiSourceFrameReader = this.kinectSensor.OpenMultiSourceFrameReader(defaultDisplaySources);

                // Color Variable Initialization
                FrameDescription colorFrameDescription = this.kinectSensor.ColorFrameSource.FrameDescription;
                this.colorImage = new byte[colorFrameDescription.Width * colorFrameDescription.Height * this.bytesPerPixel];
                this.colorImageYCC = new byte[colorFrameDescription.Width * colorFrameDescription.Height * 2];
                this.colorImageBitmap = new WriteableBitmap(colorFrameDescription.Width, colorFrameDescription.Height, 96.0, 96.0, PixelFormats.Bgr32, null);
                
                // Infrared Variable Initialization
                FrameDescription infraredFrameDescription = this.kinectSensor.InfraredFrameSource.FrameDescription;
                this.infraredFrameData = new ushort[infraredFrameDescription.Width * infraredFrameDescription.Height];
                this.infraredPixels = new byte[infraredFrameDescription.Width * infraredFrameDescription.Height * this.bytesPerPixel];
                this.infraredImageBitmap = new WriteableBitmap(infraredFrameDescription.Width, infraredFrameDescription.Height, 96.0, 96.0, PixelFormats.Bgr32, null);
                
                // Depth Variable Initialization 
                FrameDescription depthFrameDescription = this.kinectSensor.DepthFrameSource.FrameDescription;
                this.depthPixels = new byte[depthFrameDescription.Width * depthFrameDescription.Height * this.bytesPerPixel];
                this.depthFrameData = new ushort[depthFrameDescription.Width * depthFrameDescription.Height];
                this.depthImageBitmap = new WriteableBitmap(depthFrameDescription.Width, depthFrameDescription.Height, 96.0, 96.0, PixelFormats.Bgr32, null);
                
                // Body Variable Initialization
                this.coordinateMapper = this.kinectSensor.CoordinateMapper;
                this.bodies = new Body[6]; //[this.kinectSensor.BodyFrameSource.BodyCount];
                
                // Boody Index
                FrameDescription bodyIndexFrameDescription = this.kinectSensor.BodyIndexFrameSource.FrameDescription;
                this.bodyIndexFrameData = new byte[bodyIndexFrameDescription.Width * bodyIndexFrameDescription.Height];
                this.displayPixels = new byte[bodyIndexFrameDescription.Width * bodyIndexFrameDescription.Height * this.bytesPerPixel];
                this.colorPoints = new ColorSpacePoint[bodyIndexFrameDescription.Width * bodyIndexFrameDescription.Height];
                this.bodyIndexImageBitmap = new WriteableBitmap(bodyIndexFrameDescription.Width, bodyIndexFrameDescription.Height, 96.0, 96.0, PixelFormats.Bgra32, null);
                
            }
            else
            {
                // on failure, display error message
                System.Windows.MessageBox.Show("Failed to connect to the Kinect!", "No Kinect!");
            }

            // Create a drawing group
            this.drawingGroup = new DrawingGroup();

            // Create an image source that we can use in our image control
            this.bodyDrawingImage = new DrawingImage(this.drawingGroup);


            // use the window object as the view model in this simple example
            this.DataContext = this;

            // initialize the components (controls) of the window
            this.InitializeComponent();

        }

        // Image Sources for each kind of Frame: Color, Depth, Infrared, Body, BodyIndex.
        #region ImageSources
        public ImageSource colorImageSource 
        {
            get 
            {
                return this.colorImageBitmap; 
            }
        }

        public ImageSource infraredImageSource 
        {
            get 
            { 
                return this.infraredImageBitmap; 
            }
        }
        
        public ImageSource depthImageSource
        {
            get
            {
                return this.depthImageBitmap;
            }
        }
        
        public ImageSource bodyImageSource
        {
            get
            {
                return this.bodyDrawingImage;
            }
        }

        public ImageSource bodyIndexImageSource
        {
            get
            {
                return this.bodyIndexImageBitmap;
            }
        }
        #endregion

        #region Window Open/Close Functions
        /// Execute start up tasks
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {            
            if (this.displayMultiSourceFrameReader != null)
            {
                this.displayMultiSourceFrameReader.MultiSourceFrameArrived += this.Reader_DisplayMultiSourceFrameArrived;
            }
        }

        /// Execute shutdown tasks
        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            if (this.displayMultiSourceFrameReader != null)
            {
                this.displayMultiSourceFrameReader.Dispose();
                this.displayMultiSourceFrameReader = null;
            }

            if (this.recordMultiSourceFrameReader != null)
            {
                this.recordMultiSourceFrameReader.Dispose();
                this.recordMultiSourceFrameReader = null;
            }

            if (this.kinectSensor != null)
            {
                this.kinectSensor.Close();
                this.kinectSensor = null;
            }
            if (userGestureWindow != null)
            {
                userGestureWindow.Close();
            }
        }
        #endregion     
    }
}
