﻿namespace CKAKinect2Tool
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using Microsoft.Kinect;
    using Emgu.CV;
    using Emgu.CV.Structure;
    using System.Windows.Controls;
    using System.Threading;
    using System.Text;

    public partial class MainWindow : Window
    {
        #region VARIABLES

        // Number of Frames
        int record_nFrames = 0;
        string record_OutputFolderPath = null;

        // Recording Variables
        VideoWriter record_Color_vid = null;
        BinaryWriter record_Depth_bin = null;
        BinaryWriter record_Mask_bin = null;
        StringBuilder record_Skeleton_csv = null;
        StringBuilder record_Labels_csv = null;

        protected Queue<ushort[]> _depthFrameQueue = null;
        protected Queue<byte[]> _colorFrameQueue = null;
        protected Queue<byte[]> _maskFrameQueue = null;

        Thread recording_thread = null;

        #endregion

        private void record_ProcessMultiSourceFrame(Object sender, MultiSourceFrameArrivedEventArgs e)
        {

            MultiSourceFrame multiSourceFrame = e.FrameReference.AcquireFrame();

            // Check if the multi source frame is null, if so return.
            if (multiSourceFrame == null) return;

            // Get the frame references from the multisource frame
            using (BodyFrame bodyFrame = multiSourceFrame.BodyFrameReference.AcquireFrame())
            using (ColorFrame colorFrame = multiSourceFrame.ColorFrameReference.AcquireFrame())
            using (DepthFrame depthFrame = multiSourceFrame.DepthFrameReference.AcquireFrame())
            using (BodyIndexFrame bodyIndexFrame = multiSourceFrame.BodyIndexFrameReference.AcquireFrame())
            {
                // Check all frames for being null
                if (bodyFrame == null || colorFrame == null || depthFrame == null || bodyIndexFrame == null)
                {
                    return;
                }

                byte[] new_colorImage = new byte[1920 * 1080 * 4];
                ushort[] new_depthFrameData = new ushort[424 * 512];
                byte[] new_bodyIndexFrameData = new byte[424 * 512];
                
                // Increment the number of frames
                record_nFrames = record_nFrames + 1;

                #region BODY
                              
                // Copy detected bodies into an array
                bodyFrame.GetAndRefreshBodyData(this.bodies);
                int nBodies = 0;
                
                // For each body in the array
                foreach (Body body in this.bodies)
                {
                    // If the body is tracked
                    if (body.IsTracked)
                    {
                        // Save the skeleton features to the csv file and increment the number of bodies
                        record_Skeleton_csv.Append(record_getSkeletonFeatures(body));
                        nBodies++;
                    }
                }
                // Check if no bodies detected
                if (nBodies == 0) record_Skeleton_csv.Append("No Body Detected!\n");
                #endregion
                
                //Color
                colorFrame.CopyConvertedFrameDataToArray(new_colorImage, ColorImageFormat.Bgra);
                this.colorImageBitmap.WritePixels(new Int32Rect(0, 0, 1920, 1080), new_colorImage, 1920 * 4, 0);
                _colorFrameQueue.Enqueue(new_colorImage);
                //
               
                
                // Depth
                depthFrame.CopyFrameDataToArray(new_depthFrameData);
                _depthFrameQueue.Enqueue(new_depthFrameData);
                                
                // Body
                bodyIndexFrame.CopyFrameDataToArray(new_bodyIndexFrameData);
                _maskFrameQueue.Enqueue(new_bodyIndexFrameData);
                
            }
        }

        protected void Recording()
        {
            while (true)
            {
                while (_depthFrameQueue.Count > 0)
                {
                    var depthData = _depthFrameQueue.Dequeue();

                    for (int i = 0; i < depthData.Length; ++i)
                    {
                        record_Depth_bin.Write(depthData[i]);
                    }
                }

                while (_colorFrameQueue.Count > 0)
                {
                    var colorData = _colorFrameQueue.Dequeue();

                    record_colorFrameBGRA.Bytes = colorData;
                    record_colorFrameBGR = record_colorFrameBGRA.Convert<Bgr, Byte>();
                    record_Color_vid.Write(record_colorFrameBGR.Mat);
                }

                while (_maskFrameQueue.Count > 0)
                {

                    var maskData = _maskFrameQueue.Dequeue();

                    for (int i = 0; i < maskData.Length; i++)
                    {
                        record_Mask_bin.Write(maskData[i]);
                    }
                }

            }
        }

        private void record_restartKinect()
        {            
            this.kinectSensor.Close();
            this.kinectSensor = null;
            if (recordMultiSourceFrameReader != null)
            {
                this.recordMultiSourceFrameReader.Dispose();
                this.recordMultiSourceFrameReader = null;
            }

            this.kinectSensor = KinectSensor.GetDefault();

            if (this.kinectSensor != null)
            {
                // open the sensor
                this.kinectSensor.Open();

                // Set which frame sources will be used by checking the checkboxes
                FrameSourceTypes recordFrameSources = FrameSourceTypes.Color | FrameSourceTypes.Depth | FrameSourceTypes.Body | FrameSourceTypes.BodyIndex;

                // Open the recorder MultiSourceFrameReader.
                this.recordMultiSourceFrameReader = this.kinectSensor.OpenMultiSourceFrameReader(recordFrameSources);
                this.recordMultiSourceFrameReader.MultiSourceFrameArrived += this.record_ProcessMultiSourceFrame;
            }
        }

        private String record_getJointFeatures(Joint myJoint, JointOrientation myJointOrientation)
        {
            String myJointFeatures = "";

            // Feature Type
            myJointFeatures += myJoint.JointType.ToString() + ";";

            // Tracking State
            myJointFeatures += myJoint.TrackingState.ToString() + ";";

            // Coordinates
            myJointFeatures += myJoint.Position.X.ToString() + ";";
            myJointFeatures += myJoint.Position.Y.ToString() + ";";
            myJointFeatures += myJoint.Position.Z.ToString() + ";";
            
            // Orientation
            myJointFeatures += myJointOrientation.Orientation.W.ToString() + ";";
            myJointFeatures += myJointOrientation.Orientation.X.ToString() + ";";
            myJointFeatures += myJointOrientation.Orientation.Y.ToString() + ";";
            myJointFeatures += myJointOrientation.Orientation.Z.ToString() + ";";

            // Mapping to Color Space
            ColorSpacePoint colorSpacePoint = this.coordinateMapper.MapCameraPointToColorSpace(myJoint.Position);
            myJointFeatures += colorSpacePoint.X.ToString() + ";";
            myJointFeatures += colorSpacePoint.Y.ToString() + ";";

            // Mapping to Depth Space
            DepthSpacePoint depthSpacePoint = this.coordinateMapper.MapCameraPointToDepthSpace(myJoint.Position);
            myJointFeatures += depthSpacePoint.X.ToString() + ";";
            myJointFeatures += depthSpacePoint.Y.ToString() + ";";
            
            return myJointFeatures;
        }

        private String record_getSkeletonFeatures(Body myBody)
        {
            String mySkeletonFeatures = "";

            IReadOnlyDictionary<JointType, Joint> myJoints = myBody.Joints;
            IReadOnlyDictionary<JointType, JointOrientation> myJointOrientations = myBody.JointOrientations;

            for (JointType i = JointType.SpineBase; i <= JointType.ThumbRight; i++)
            {
                mySkeletonFeatures += record_getJointFeatures(myJoints[i], myJointOrientations[i]);
            }

            // Hand Status and Confidience
            mySkeletonFeatures += "HandLeftState" + ";" + myBody.HandLeftState.ToString() + ";" + myBody.HandLeftConfidence.ToString() + ";";
            mySkeletonFeatures += "HandRightState" + ";" + myBody.HandRightState.ToString() + ";" + myBody.HandRightConfidence.ToString() + ";";
                        
            // Lean Feature
            mySkeletonFeatures += "Lean" + ";" + myBody.LeanTrackingState.ToString() + ";";
            mySkeletonFeatures += myBody.Lean.X.ToString() + ";" + myBody.Lean.Y.ToString() + "\n";

            return mySkeletonFeatures;
        }
        
        public void record_startRecordingFunction()
        {   
            if (this.ui_script == null || this.userGestureWindow == null || this.ui_userList == null)
            {
                System.Windows.MessageBox.Show("Before recording please:\n"+"1- Load the Script\n"+"2- Load the User List\n"+"3- Open the User Window", "Not Ready for Recording!");
                return;
            }

            record_Skeleton_csv = new StringBuilder();
            record_Labels_csv = new StringBuilder();
            
            _depthFrameQueue = new Queue<ushort[]>();
            _colorFrameQueue = new Queue<byte[]>();
            _maskFrameQueue = new Queue<byte[]>();

            recording_thread = new Thread(Recording);
            recording_thread.Start();

            // Disable Recording Options
            this.tbBrowseScript.IsEnabled = false;
            this.tbBrowseUserList.IsEnabled = false;
            this.btnLoadScript.IsEnabled = false;
            this.btnLoadUserList.IsEnabled = false;
            this.cmbUserList.IsEnabled = false;
            this.btnOpenUserWindow.IsEnabled = false;

            // Enable Recording Controllers
            this.btnStartGesture.IsEnabled = true;
            this.btnStopGesture.IsEnabled = true;
            this.btnNextGesture.IsEnabled = true;
            this.btnRepeatGesture.IsEnabled = true;
            this.btnInvalidGesture.IsEnabled = true;
            this.btnPrevGesture.IsEnabled = true;

            // Init number of frames
            this.record_nFrames = 0;
            
            // Change the color of controllers
            this.gridRecordControls.Background = Brushes.Orange;
            this.userGestureWindow.gestureGrid.Background = Brushes.Orange;
                        
            //* Needs to be changed into the bosphorus.sign.userxx.dd.mm.yy.hh.mm
            String time = DateTime.Now.ToString("ddMMyyyy.HHmmss");
            String userNo =  this.ui_users[cmbUserList.SelectedIndex].ID;
            this.record_OutputFolderPath = "C:\\Bosphorus Sign\\bosphorus.sign." + this.ui_scriptID + "." + userNo + "." + time + "\\";
            Directory.CreateDirectory(record_OutputFolderPath);

            // Video Recording Function
            FrameDescription colorFrameDescription = this.kinectSensor.ColorFrameSource.FrameDescription;
            this.record_colorFrameBGR = new Emgu.CV.Image<Bgr, Byte>(colorFrameDescription.Width, colorFrameDescription.Height);
            this.record_colorFrameBGRA = new Emgu.CV.Image<Bgra, Byte>(colorFrameDescription.Width, colorFrameDescription.Height);
            System.Drawing.Size colorframeSize = new System.Drawing.Size(colorFrameDescription.Width, colorFrameDescription.Height);
                        
            this.record_Color_vid = new VideoWriter(record_OutputFolderPath + "color.avi", 30, colorframeSize, true);
            //this.record_Color_vid = new VideoWriter(record_OutputFolderPath + "color.avi", VideoWriter.Fourcc('D', 'I', 'V', 'X'), 30, colorframeSize, true);
            //this.record_Color_vid = new VideoWriter(record_OutputFolderPath + "color.avi", -1, 30, colorframeSize, true);
            
            // Dispose the display MultiSourceFrameReader
            if (displayMultiSourceFrameReader != null)
            {
                this.displayMultiSourceFrameReader.Dispose();
                this.displayMultiSourceFrameReader = null;
            }

            Thread.Sleep(100);

            // Set which frame sources will be used by checking the checkboxes
            FrameSourceTypes recordFrameSources = FrameSourceTypes.Color | FrameSourceTypes.Depth | FrameSourceTypes.Body | FrameSourceTypes.BodyIndex;

            // Open the recorder MultiSourceFrameReader.
            this.recordMultiSourceFrameReader = this.kinectSensor.OpenMultiSourceFrameReader(recordFrameSources);
            this.recordMultiSourceFrameReader.MultiSourceFrameArrived += this.record_ProcessMultiSourceFrame;

            // Crete Binary Files
            record_Depth_bin = new BinaryWriter(File.Open(record_OutputFolderPath + "depth.bin", FileMode.Create));
            record_Mask_bin = new BinaryWriter(File.Open(record_OutputFolderPath + "mask.bin", FileMode.Create));

            // Clear the previous
            record_Skeleton_csv.Clear();
            record_Labels_csv.Clear();

            // Raise the recording flag.
            ui_IsRecording = true;
            btnRecord.Content = "Stop Recording";
            imgDisplayImage.Source = colorImageSource;
        }

        public void record_stopRecordingFunction()
        {
            recording_thread.Abort();
            recording_thread = null;

            while (_depthFrameQueue.Count > 0)
            {
                var depthData = _depthFrameQueue.Dequeue();

                for (int i = 0; i < depthData.Length; ++i)
                {
                    record_Depth_bin.Write(depthData[i]);
                }
            }
            if (_depthFrameQueue != null)
            {
                _depthFrameQueue.Clear();
                _depthFrameQueue = null;
            }

            while (_colorFrameQueue.Count > 0)
            {
                var colorData = _colorFrameQueue.Dequeue();

                record_colorFrameBGRA.Bytes = colorData;
                record_colorFrameBGR = record_colorFrameBGRA.Convert<Bgr, Byte>();
                record_Color_vid.Write(record_colorFrameBGR.Mat);
            }
            if (_colorFrameQueue != null)
            {
                _colorFrameQueue.Clear();
                _colorFrameQueue = null;
            }

            while (_maskFrameQueue.Count > 0)
            {
                var maskData = _maskFrameQueue.Dequeue();

                for (int i = 0; i < maskData.Length; i++)
                {
                    record_Mask_bin.Write(maskData[i]);
                }
            }
            if (_maskFrameQueue != null)
            {
                _maskFrameQueue.Clear();
                _maskFrameQueue = null;
            }
            
            // Enable Recordng Options
            this.tbBrowseScript.IsEnabled = true;
            this.tbBrowseUserList.IsEnabled = true;
            this.btnLoadScript.IsEnabled = true;
            this.btnLoadUserList.IsEnabled = true;
            this.cmbUserList.IsEnabled = true;
            this.btnOpenUserWindow.IsEnabled = true;

            // Disable Recording Controllers
            this.btnStartGesture.IsEnabled = false;
            this.btnStopGesture.IsEnabled = false;
            this.btnNextGesture.IsEnabled = false;
            this.btnRepeatGesture.IsEnabled = false;
            this.btnInvalidGesture.IsEnabled = false;
            this.btnPrevGesture.IsEnabled = false;

            // Change the Recorder Indicating Colors Back to Normal
            this.gridRecordControls.Background = Brushes.White;
            this.userGestureWindow.gestureGrid.Background = Brushes.White;

            // Dispose the Recording MultiSourceFrameReader.
            if (recordMultiSourceFrameReader != null)
            {
                this.recordMultiSourceFrameReader.Dispose();
                this.recordMultiSourceFrameReader = null;
            }

            // Initialize the Display MultiSourceFrameReader back.
            this.displayMultiSourceFrameReader = this.kinectSensor.OpenMultiSourceFrameReader(FrameSourceTypes.Color | FrameSourceTypes.Depth | FrameSourceTypes.Infrared |
                                                                                              FrameSourceTypes.Body | FrameSourceTypes.BodyIndex);
            this.displayMultiSourceFrameReader.MultiSourceFrameArrived += this.Reader_DisplayMultiSourceFrameArrived;

            // Change the video playback back to the Display Mode
            if (cbDrawBody.IsChecked == true)
            {
                this.imgDisplayImage.Source = this.bodyImageSource;
            }

            // Write the Body Joints to CSV File [This will be change to Binary File]
            File.WriteAllText(record_OutputFolderPath + "body.csv", record_Skeleton_csv.ToString());
            record_Skeleton_csv.Clear();
            record_Skeleton_csv = null;

            // Write the Sign Borders to CSV File
            File.WriteAllText(record_OutputFolderPath + "borders.csv", record_Labels_csv.ToString());
            record_Labels_csv.Clear();
            record_Labels_csv = null;

            // Dispose the video writer objects [This is essantial]
            if (record_Color_vid != null)
            {
                record_Color_vid.Dispose();
                record_Color_vid = null;
            }

            // Check if the user wants to compress the video
            if (cbCompressColor.IsChecked == true) { 
                // Create a process for 
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();

                startInfo.WorkingDirectory = record_OutputFolderPath;
                startInfo.FileName = "cmd.exe";

                // Fastest Lossless Compresion
                startInfo.Arguments = "/C ffmpeg -i color.avi -c:v libx264 -preset ultrafast -qp 0 color.mkv & del color.avi";

                process.StartInfo = startInfo;
                process.Start();
            }

            // Lower the recording flag.
            ui_IsRecording = false;

            // Change the Record Button Text to "Start Recording"
            btnRecord.Content = "Start Recording";

            // Set the number of Frames to Zero [Not Really Neccesarry Since We Zero it at the record_start_fuction]
            record_nFrames = 0;

            // Dispose The Frames [Image<>]
            if (record_colorFrameBGR != null)
            {
                record_colorFrameBGR.Dispose();
                record_colorFrameBGR = null;
            }

            if (record_colorFrameBGRA != null) 
            { 
                record_colorFrameBGRA.Dispose();
                record_colorFrameBGRA = null;
            }

            record_Depth_bin.Close();
            record_Depth_bin.Dispose();
            record_Depth_bin = null;

            record_Mask_bin.Close();
            record_Mask_bin.Dispose();
            record_Mask_bin = null;

        }

    }
   
}