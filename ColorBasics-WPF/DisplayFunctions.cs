﻿
namespace CKAKinect2Tool
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using Microsoft.Kinect;
    using Emgu.CV;
    using Emgu.CV.Structure;
    using System.Windows.Controls;
    using System.Threading;

    public partial class MainWindow : Window
    {

        private void Reader_DisplayMultiSourceFrameArrived(Object sender, MultiSourceFrameArrivedEventArgs e)
        {
            MultiSourceFrameReference multiSourceFrameReference = e.FrameReference;

            MultiSourceFrame multiSourceFrame = multiSourceFrameReference.AcquireFrame();

            if (multiSourceFrame == null) return;

            // Get FrameReferences for each source
            ColorFrameReference colorFrameReference = multiSourceFrame.ColorFrameReference;
            DepthFrameReference depthFrameReference = multiSourceFrame.DepthFrameReference;
            InfraredFrameReference infraredFrameReference = multiSourceFrame.InfraredFrameReference;
            BodyFrameReference bodyFrameReference = multiSourceFrame.BodyFrameReference;
            BodyIndexFrameReference bodyIndexFrameReference = multiSourceFrame.BodyIndexFrameReference;


            using (ColorFrame colorFrame = colorFrameReference.AcquireFrame())
            {
                using (DepthFrame depthFrame = depthFrameReference.AcquireFrame())
                {
                    using (InfraredFrame infraredFrame = infraredFrameReference.AcquireFrame())
                    {
                        using (BodyFrame bodyFrame = bodyFrameReference.AcquireFrame())
                        {
                            using (BodyIndexFrame bodyIndexFrame = bodyIndexFrameReference.AcquireFrame())
                            {
                                if (colorFrame == null || depthFrame == null || infraredFrame == null ||
                                    bodyFrame == null || bodyIndexFrame == null) return;


                                FrameDescription colorFrameDescription = colorFrame.FrameDescription;
                                FrameDescription infraredFrameDescription = infraredFrame.FrameDescription;
                                FrameDescription depthFrameDescription = depthFrame.FrameDescription;
                                FrameDescription bodyIndexFrameDescription = bodyIndexFrame.FrameDescription;

                                #region colorFrame processing

                                // verify data and write the new color frame data to the display bitmap
                                if ((colorFrameDescription.Width == this.colorImageBitmap.PixelWidth) &&
                                    (colorFrameDescription.Height == this.colorImageBitmap.PixelHeight))
                                {
                                    if (colorFrame.RawColorImageFormat == ColorImageFormat.Bgra)
                                    {
                                        colorFrame.CopyRawFrameDataToArray(this.colorImage);
                                    }
                                    else
                                    {
                                        colorFrame.CopyConvertedFrameDataToArray(this.colorImage, ColorImageFormat.Bgra);
                                    }

                                    this.colorImageBitmap.WritePixels(
                                        new Int32Rect(0, 0, colorFrameDescription.Width, colorFrameDescription.Height),
                                        this.colorImage,
                                        colorFrameDescription.Width * this.bytesPerPixel,
                                        0);
                                }
                                #endregion

                                #region infraredFrame processing

                                if (((infraredFrameDescription.Width * infraredFrameDescription.Height) == this.infraredFrameData.Length) &&
                                    (infraredFrameDescription.Width == this.infraredImageBitmap.PixelWidth) &&
                                    (infraredFrameDescription.Height == this.infraredImageBitmap.PixelHeight))
                                {
                                    // Copy the pixel data from the image to a temporary array
                                    infraredFrame.CopyFrameDataToArray(this.infraredFrameData);

                                    // Convert the infrared to RGB
                                    int colorPixelIndex = 0;

                                    for (int i = 0; i < this.infraredFrameData.Length; ++i)
                                    {
                                        // Get the infrared value for this pixel
                                        ushort ir = this.infraredFrameData[i];

                                        // To convert to a byte, we're discarding the most-significant
                                        // rather than least-significant bits.
                                        // We're preserving detail, although the intensity will "wrap."

                                        // To convert to a byte, we're discarding the least-significant bits.
                                        byte intensity = (byte)(ir >> 8);

                                        // Write out blue byte
                                        this.infraredPixels[colorPixelIndex++] = intensity;

                                        // Write out green byte
                                        this.infraredPixels[colorPixelIndex++] = intensity;

                                        // Write out red byte                        
                                        this.infraredPixels[colorPixelIndex++] = intensity;

                                        // We're outputting BGR, the last byte in the 32 bits is unused so skip it
                                        // If we were outputting BGRA, we would write alpha here.
                                        ++colorPixelIndex;
                                    }

                                    this.infraredImageBitmap.WritePixels(
                                        new Int32Rect(0, 0, infraredFrameDescription.Width, infraredFrameDescription.Height),
                                        this.infraredPixels,
                                        infraredFrameDescription.Width * this.bytesPerPixel,
                                        0);
                                }


                                #endregion

                                #region depthFrame processing

                                // verify data and write the new depth frame data to the display bitmap
                                if (((depthFrameDescription.Width * depthFrameDescription.Height) == this.depthFrameData.Length) &&
                                    (depthFrameDescription.Width == this.depthImageBitmap.PixelWidth) &&
                                    (depthFrameDescription.Height == this.depthImageBitmap.PixelHeight))
                                {
                                    // Copy the pixel data from the image to a temporary array
                                    depthFrame.CopyFrameDataToArray(this.depthFrameData);

                                    // Get the min and max reliable depth for the current frame
                                    ushort minDepth = depthFrame.DepthMinReliableDistance;
                                    ushort maxDepth = depthFrame.DepthMaxReliableDistance;

                                    // Convert the depth to RGB
                                    int colorPixelIndex = 0;

                                    for (int i = 0; i < this.depthFrameData.Length; ++i)
                                    {
                                        // Get the depth for this pixel
                                        ushort depth = this.depthFrameData[i];

                                        // To convert to a byte, we're discarding the most-significant
                                        // rather than least-significant bits.
                                        // We're preserving detail, although the intensity will "wrap."
                                        // Values outside the reliable depth range are mapped to 0 (black).
                                        int MapDepthToByte = 8000 / 256;
                                        byte intensity = (byte)(depth >= minDepth && depth <= maxDepth ? (depth / MapDepthToByte) : 0); 
                                        
                                        //(byte)(depth / (8000 / 256));//(depth >= minDepth && depth <= maxDepth ? depth : 0);

                                        // Write out blue byte
                                        this.depthPixels[colorPixelIndex++] = intensity;

                                        // Write out green byte
                                        this.depthPixels[colorPixelIndex++] = intensity;

                                        // Write out red byte                        
                                        this.depthPixels[colorPixelIndex++] = intensity;

                                        // We're outputting BGR, the last byte in the 32 bits is unused so skip it
                                        // If we were outputting BGRA, we would write alpha here.
                                        ++colorPixelIndex;
                                    }

                                    this.depthImageBitmap.WritePixels(
                                        new Int32Rect(0, 0, depthFrameDescription.Width, depthFrameDescription.Height),
                                        this.depthPixels,
                                        depthFrameDescription.Width * this.bytesPerPixel,
                                        0);

                                }
                                #endregion

                                #region BodyIndexFrame processing

                                int depthWidth = depthFrameDescription.Width;
                                int depthHeight = depthFrameDescription.Height;

                                int colorWidth = colorFrameDescription.Width;
                                int colorHeight = colorFrameDescription.Height;

                                int bodyIndexWidth = bodyIndexFrameDescription.Width;
                                int bodyIndexHeight = bodyIndexFrameDescription.Height;

                                // verify data and write the new registered frame data to the display bitmap
                                if ((bodyIndexWidth * bodyIndexHeight) == this.bodyIndexFrameData.Length)
                                {

                                    bodyIndexFrame.CopyFrameDataToArray(this.bodyIndexFrameData);

                                    this.coordinateMapper.MapDepthFrameToColorSpace(this.depthFrameData, this.colorPoints);

                                    Array.Clear(this.displayPixels, 0, this.displayPixels.Length);

                                    // loop over each row and column of the depth
                                    for (int y = 0; y < depthHeight; ++y)
                                    {
                                        for (int x = 0; x < depthWidth; ++x)
                                        {
                                            // calculate index into depth array
                                            int depthIndex = (y * depthWidth) + x;

                                            byte player = this.bodyIndexFrameData[depthIndex];

                                            // if we're tracking a player for the current pixel, sets its color and alpha to full
                                            if (player != 0xff)
                                            {

                                                // retrieve the depth to color mapping for the current depth pixel
                                                ColorSpacePoint colorPoint = this.colorPoints[depthIndex];

                                                // make sure the depth pixel maps to a valid point in color space
                                                int colorX = (int)Math.Floor(colorPoint.X + 0.5);
                                                int colorY = (int)Math.Floor(colorPoint.Y + 0.5);
                                                if ((colorX >= 0) && (colorX < colorWidth) && (colorY >= 0) && (colorY < colorHeight))
                                                {
                                                    // calculate index into color array
                                                    int colorIndex = ((colorY * colorWidth) + colorX) * this.bytesPerPixel;

                                                    // set source for copy to the color pixel
                                                    int displayIndex = depthIndex * this.bytesPerPixel;
                                                    this.displayPixels[displayIndex] = this.colorImage[colorIndex];
                                                    this.displayPixels[displayIndex + 1] = this.colorImage[colorIndex + 1];
                                                    this.displayPixels[displayIndex + 2] = this.colorImage[colorIndex + 2];
                                                    this.displayPixels[displayIndex + 3] = 0xff;
                                                }
                                            }
                                        }
                                    }

                                    this.bodyIndexImageBitmap.WritePixels(
                                        new Int32Rect(0, 0, depthWidth, depthHeight),
                                        this.displayPixels,
                                        depthWidth * this.bytesPerPixel,
                                        0);
                                }

                                #endregion

                                #region BodyFrame processing

                                if (cbDrawBody.IsChecked == true)
                                {
                                    using (DrawingContext dc = this.drawingGroup.Open())
                                    {
                                        if (rbColor.IsChecked == true)
                                        {
                                            dc.DrawImage(colorImageSource, new Rect(0.0, 0.0, colorFrameDescription.Width, colorFrameDescription.Height));
                                        }
                                        else if (rbDepth.IsChecked == true)
                                        {
                                            dc.DrawImage(depthImageSource, new Rect(0.0, 0.0, depthFrameDescription.Width, depthFrameDescription.Height));
                                        }
                                        else if (rbInfrared.IsChecked == true)
                                        {
                                            dc.DrawImage(infraredImageSource, new Rect(0.0, 0.0, infraredFrameDescription.Width, infraredFrameDescription.Height));
                                        }
                                        else if (rbBodyIndex.IsChecked == true)
                                        {
                                            dc.DrawImage(bodyIndexImageSource, new Rect(0.0, 0.0, bodyIndexFrameDescription.Width, bodyIndexFrameDescription.Height));
                                        }

                                        // The first time GetAndRefreshBodyData is called, Kinect will allocate each Body in the array.
                                        // As long as those body objects are not disposed and not set to null in the array,
                                        // those body objects will be re-used.
                                        bodyFrame.GetAndRefreshBodyData(this.bodies);

                                        foreach (Body body in this.bodies)
                                        {
                                            if (body.IsTracked)
                                            {
                                                IReadOnlyDictionary<JointType, Joint> joints = body.Joints;

                                                // convert the joint points to depth (display) space
                                                Dictionary<JointType, Point> jointPoints = new Dictionary<JointType, Point>();
                                                foreach (JointType jointType in joints.Keys)
                                                {
                                                    if (rbColor.IsChecked == true)
                                                    {
                                                        ColorSpacePoint colorSpacePoint = this.coordinateMapper.MapCameraPointToColorSpace(joints[jointType].Position);
                                                        jointPoints[jointType] = new Point(colorSpacePoint.X, colorSpacePoint.Y);
                                                    }
                                                    else
                                                    {
                                                        DepthSpacePoint depthSpacePoint = this.coordinateMapper.MapCameraPointToDepthSpace(joints[jointType].Position);
                                                        jointPoints[jointType] = new Point(depthSpacePoint.X, depthSpacePoint.Y);
                                                    }
                                                }

                                                this.DrawBody(joints, jointPoints, dc);

                                                this.DrawHand(body.HandLeftState, jointPoints[JointType.HandLeft], dc);
                                                this.DrawHand(body.HandRightState, jointPoints[JointType.HandRight], dc);
                                            }
                                        }

                                        if (rbColor.IsChecked == true)
                                        {
                                            // prevent drawing outside of our render area
                                            this.drawingGroup.ClipGeometry = new RectangleGeometry(new Rect(0.0, 0.0, colorFrameDescription.Width, colorFrameDescription.Height));
                                        }
                                        else
                                        {
                                            this.drawingGroup.ClipGeometry = new RectangleGeometry(new Rect(0.0, 0.0, depthFrameDescription.Width, depthFrameDescription.Height));
                                        }
                                    }
                                }
                                #endregion

                            }
                        }
                    }
                }
            }
        }


    }
}