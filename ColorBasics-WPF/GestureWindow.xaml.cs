﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CKAKinect2Tool
{   
    /// <summary>
    /// Interaction logic for myGestureWindow.xaml
    /// </summary>
    public partial class GestureWindow : Window
    {
        public GestureWindow(VisualBrush visualBrush)
        {
            InitializeComponent();
            rectUserGesturePlayer.Fill = visualBrush;
        }

        public void green()
        {
            gestureGrid.Background = Brushes.Green;
        }

        public void orange()
        {
            gestureGrid.Background = Brushes.Orange;
        }

        public void red()
        {
            gestureGrid.Background = Brushes.Red;
        }
    }
}
